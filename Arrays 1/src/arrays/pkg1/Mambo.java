//Autor Ricardo Sanchez//
//Cree una funcion que tome un arreglo de numeros de un arreglo y regreso "Maaaambo",
//si en el arreglo existe un digito 7, si no aparecen que regrese "Aqui no se baila Mambo !!!".
package arrays.pkg1;

public class Mambo {

  public static void main(String[] args) {
        System.out.println("Ricardo Daniel Sanchez Rosario, 63876");
        //iniciamos los arreglos
        
         int[] Arr1 = {1,2,3,4,5,6,7};            //Arreglo 1 "Maaaambo"            
         int[] Arr2 = {8,6,33,100};               //Arreglo 2 "Aquí no se baila Mambo !!!"
         int[] Arr3 = {2,55,60,97,86};            //Arreglo 3 "Maaaambo"
                
         System.out.println("Arreglo 1");           
         LeerArray(Arr1);                     //Utiliza para verficar si cuenta con el 7
         
         System.out.println("\nArreglo 2");
         LeerArray(Arr2);                     //Utiliza para verficar si cuenta con el 7
         
         System.out.println("\nArreglo 3");
         LeerArray(Arr3);                     //Utiliza para verficar si cuenta con el 7
         
    }
    
    //funcion para leer los arreglos e imprimr el mensaje
    public static void LeerArray(int[] NombArreglo){
       String var = "";                                                   //declaro variable para concatenar
       
        for(int i=0;i<NombArreglo.length;i++){                            //ciclo para leer los arreglos                
           var = var + NombArreglo[i];
               
        }
        
       if(var.indexOf("7")== -1){                                          //metodo para valorar si esta el 7
             System.out.println("Aquí no se baila Mambo !!!"); 
                
            }else{
              
                System.out.println("Maaaambo");
             }    
    }
    
    
}
