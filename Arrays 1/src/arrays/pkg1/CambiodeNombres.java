//Autor Ricardo Sanchez//
//Reemplazar los nombre de 3 de sus compañeros y su edad por el de otros 
//compañeros e imprimir el arreglo con cada uno de los registros.//
package arrays.pkg1;
public class CambiodeNombres {
public static void main(String[] args) {
System.out.println("Ricardo Dabiel Sanchez Rosario, 63876");
System.out.println("Matriz de nombres");
int iPosicion1, iPosicion2;
String[][] m;
m = new String[6][1];
m[0][0] = "Nombres";
m[1][0] = "Juan Pablo";
m[2][0] = "Frida";
m[3][0] = "Jose";
m[4][0] = "Eduardo";
m[5][0] = "Martin";
System.out.println("");
System.out.print(m[0][0]+ " | ");
System.out.println("");
System.out.print(m[1][0]+ " | ");
System.out.println("");
System.out.print(m[2][0]+ " | ");
System.out.println("");
System.out.print(m[3][0]+ " | ");
System.out.println("");
System.out.print(m[4][0]+ " | ");
System.out.println("");
System.out.print(m[5][0]+ " | ");
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("Matriz de edades");
int [][] m2;
m2 = new int[5][1];
m2[0][0] = 20;
m2[1][0] = 19;
m2[2][0] = 21;
m2[3][0] = 19;
m2[4][0] = 20;
System.out.println("");
System.out.print(m2[0][0]+ " | ");
System.out.println("");
System.out.print(m2[1][0]+ " | ");
System.out.println("");
System.out.print(m2[2][0]+ " | ");
System.out.println("");
System.out.print(m2[3][0]+ " | ");
System.out.println("");
System.out.print(m2[4][0]+ " | ");
System.out.println("");
}
}