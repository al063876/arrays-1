//Autor Ricardo Sanchez//
//Escribir en un arreglo de tipo string los nombres de pila de cinco de sus 
//compañeros y en otro arreglo de tipo numérico escribir su edad de cada uno de 
//ellos, imprimir en pantalla cada uno de los arreglos//
package arrays.pkg1;
public class Nombres {
public static void main(String[] args) {
System.out.println("Ricardo Daniel Sanchez Rosario, 63876");
System.out.println("Matriz de nombres");
int iPosicion1, iPosicion2;

String[][] m;
m = new String[6][1];
m[0][0] = "Nombres" ;
m[1][0] = "Karen" ;
m[2][0] = "Frida" ;
m[3][0] = "William" ;
m[4][0] = "Eduardo" ;
m[5][0] = "Braulio";
System.out.println("");
System.out.print(m[0][0]+ "|" );
System.out.println("");
System.out.print(m[1][0]+ "|" );
System.out.println("");
System.out.print(m[2][0]+ "|" );
System.out.println("");
System.out.print(m[3][0]+ "|" );
System.out.println("");
System.out.print(m[4][0]+ "|");
System.out.println("");
System.out.print(m[5][0]+ "|" );
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("Matriz de edades");
int [][] m2;
m2 = new int[5][1];

m2[0][0] = 18;
m2[1][0] = 18;
m2[2][0] = 19;
m2[3][0] = 18;
m2[4][0] = 19;
System.out.println("");
System.out.print(m2[0][0]+ "|" );
System.out.println("");
System.out.print(m2[1][0]+ "|" );
System.out.println("");
System.out.print(m2[2][0]+ "|" );
System.out.println("");
System.out.print(m2[3][0]+ "|" );
System.out.println("");
System.out.print(m2[4][0]+ "|" );
System.out.println("");
}
}
