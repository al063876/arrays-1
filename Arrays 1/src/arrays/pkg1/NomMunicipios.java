
package arrays.pkg1;

import java.util.Scanner;

public class NomMunicipios {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String[] CodMun = {
            "001 CALKINI", "002 CAMPECHE", "003 CARMEN",
            "004 CHAMPOTON", "005 HECELCHAKAN", "006 HOPELCHEN", 
            "007 PALIZADA", "008 TENABO", "009 ESCARCEGA", 
            "010 CALAKMUL", "011 CANDELARIA", "012 SEYBAPLAYA"};
        
        String[] CabMun = {
            "CALKINI", "SAN FRABNCISCO DE CAMPECHE", "CIUDAD DEL CARMEN", 
            "CHAMPOTON", "HECELCHAKAN", "HOPELCHEN", "PALIZADA", "TENABO", 
            "ESCARCEGA", "XPUJIL", "CANDELARIA", "SEYBAPLAYA"};
        
        int[] NumHabi = {
            52890, 259005, 221094, 83021, 28306, 37777, 
            8352, 10665, 54184, 26882, 41194, 15420};
        
         for (int i = 0; i < CodMun.length; i++) {
             System.out.print(CodMun[i]+ "  ");
             System.out.println(CabMun[i]+ " numero de habitantes "+ NumHabi[i]);
         }
         Scanner municipio = new Scanner (System.in);
  System.out.println("ingrese un numero");
  int numero = 0;
  while ( numero >12 || numero<1){
   numero =municipio.nextInt(); 
   
    switch(numero){
     case 1:
     System.out.println("001-CALKINI CALKINI 52890");
     System.out.println("NO TIENE ANTESESOR");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("002-CAMPEHCE SAN FRANCISCO DE CAMPECHE 259005");
     break;
    case 2:
     System.out.println("002-CAMPEHCE SAN FRANCISCO DE CAMPECHE 259005");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("001-CALKINI CALKINI 52890");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("003-CARMEN CIUDAD DEL CARMEN 221094");
     break;
    case 3:
     System.out.println("003-CARMEN CIUDAD DEL CARMEN 221094");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("002-CAMPEHCE SAN FRANCISCO DE CAMPECHE 259005");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("004-CHAMPOTON CHAMPOTON 83021");
     break;
    case 4:
     System.out.println("004-CHAMPOTON CHAMPOTON 83021");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("003-CARMEN CIUDAD DEL CARMEN 221094");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("005-HECELCHAKAN HECELCHAKAN 28306");
     break;
    case 5:
     System.out.println("005-HECELCHAKAN HECELCHAKAN 28306");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("004-CHAMPOTON CHAMPOTON 83021");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("006-HOPELCHEN HOPELCHEN 37777");
     break;
    case 6:
     System.out.println("006-HOPELCHEN HOPELCHEN 37777");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("005-HECELCHAKAN HECELCHAKAN 28306");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("007-PALIZADA PALIZADA 8352");
     break;
    case 7:
     System.out.println("007-PALIZADA PALIZADA 8352");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("006-HOPELCHEN HOPELCHEN 37777");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("008-TENABO TENABO 10665");
     break;
      case 8:
     System.out.println("008-TENABO TENABO 10665");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("007-PALIZADA PALIZADA 8352");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("009-ESCARCEGA ESCARCEGA 54184");
     break;
      case 9:
     System.out.println("009-ESCARCEGA ESCARCEGA 54184");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("008-TENABO TENABO 10665");
     System.out.println("MUNICIPIO POSTERIOR"); 
     System.out.println("010-CALAKMUL XPUJIL 26882");
     break;
      case 10:
     System.out.println("010-CALAKMUL XPUJIL 26882");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("009-ESCARCEGA ESCARCEGA 54184");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("011-CANDELARIA CANDELARIA 41194");
     break;
      case 11:
     System.out.println("011-CANDELARIA CANDELARIA 41194");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("010-CALAKMUL XPUJIL 26882");
     System.out.println("MUNICIPIO POSTERIOR");
     System.out.println("012-SEYBAPLAYA SEYBAPLAYA 15420");
     break;
      case 12:
     System.out.println("012-SEYBAPLAYA SEYBAPLAYA 15420");
     System.out.println("MUNICIPIO ANTESESOR");
     System.out.println("011-CANDELARIA CANDELARIA 41194");
     System.out.println("MUNICIPIO POSTERIOR NO EXISTE");
     break;
    default:
     System.out.println("numero INEXISTENTE, ingreso otro numero entre fuera del rango");
     break;
     
     
    }
    
  }
    }
}
