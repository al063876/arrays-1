//Autor Ricardo Sanchez//
//crear un arreglo de 10 posiciones y llenarlo con numeros de 1 al 10, sacar su 
//su suma y su promedio//
package arrays.pkg1;

import java.util.*;
public class Posiciones {
public static void main(String[] args) {
    System.out.println("Ricardo Daniel Sanchez Rosario, 63876");
Scanner sc = new Scanner(System.in);
int i;
int pos = 0, neg = 0; //contadores de los números positivos y negativos
int[] numeros = new int[10]; //array que contendrá los números leídos por teclado3
double sumaPos = 0, sumaNeg = 0; //acumuladores para las sumas de positivos y negativos
System.out.println("Ricardo Daniel Sanhez Rosario, 63876");
System.out.println("Lectura de los elementos de la matriz: ");
for (i = 0; i < 10; i++) {
System.out.print("numeros[" + i + "]= ");
numeros[i]=sc.nextInt();
}
//recorrer el array para sumar por separado los números positivos
// y los negativos
for (i = 0; i < 10; i++) {
if (numeros[i] > 0){ //sumar positivos
sumaPos += numeros[i];
pos++;
} else if (numeros[i] < 0){ //sumar negativos
sumaNeg += numeros[i];
neg++;
}
}
//Calcular y mostrar las medias
if (pos != 0) {
System.out.println("Media de los valores positivos: " + sumaPos / pos);
} else {
System.out.println("No ha introducido numeros positivos");
}
if (neg != 0) {
System.out.println("Media de los valores negativos: " + sumaNeg / neg);
} else {
System.out.println("No ha introducido numeros negativos");
}
}
}