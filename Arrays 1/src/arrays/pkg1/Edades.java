//Autor Ricardo Sanchez//
//Realizar la suma de las edades de sus compañeros e imprimir su total y 
//su promedio de edad//
package arrays.pkg1;
public class Edades {
public static void main(String[] args) {
System.out.println("Ricardo Daniel Sanchez Rosario");
System.out.println("Matriz de nombres");
int iPosicion1, iPosicion2;
String[][] m;
m = new String[6][1];
m[0][0] = "Nombres" ;
m[1][0] = "karen" ;
m[2][0] = "frida" ;
m[3][0] = "william";
m[4][0] = "eduardo";
m[5][0] = "braulio" ;
System.out.println("");
System.out.print(m[0][0]+ "|" );
System.out.println("");

System.out.print(m[1][0]+ "|" );
System.out.println("");
System.out.print(m[2][0]+ "|");
System.out.println("");
System.out.print(m[3][0]+ "|");
System.out.println("");
System.out.print(m[4][0]+ "|");
System.out.println("");
System.out.print(m[5][0]+ "|");
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("");
System.out.println("Matriz de edades");
int [][] m2;
m2 = new int[5][1];
m2[0][0] = 19;
m2[1][0] = 18;
m2[2][0] = 18;
m2[3][0] = 18;
m2[4][0] = 19;
System.out.println("");
System.out.print(m2[0][0]+ "|");
System.out.println("");
System.out.print(m2[1][0]+ "|");
System.out.println("");
System.out.print(m2[2][0]+ "|");
System.out.println("");
System.out.print(m2[3][0]+ "|");

System.out.println("");
System.out.print(m2[4][0]+ "|");
System.out.println("");
System.out.println("El promedio de la edad de mis amigos es");
int suma= (m2[0][0]+m2[1][0]+m2[2][0]+m2[3][0]+m2[4][0]);
int promedio = suma/4;
System.out.println(promedio);
}
}
